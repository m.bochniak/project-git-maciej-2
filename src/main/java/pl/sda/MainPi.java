package pl.sda;

import java.util.Scanner;

public class MainPi {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        float x = sc.nextFloat();

        Pi obw = new Pi();
        System.out.printf("%.7f\n", obw.obwod1(x));
        System.out.printf("%.7f\n", obw.obwod2(x));
    }
}

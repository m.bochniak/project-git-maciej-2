package pl.sda;

public class Pi {
    private float srednica;
    private float pi1 = 3.14f;
    private float pi2 = (float) Math.PI;
    private float obwod1;
    private float obwod2;

    public float obwod1(float srednica) {
        obwod1 = pi1 * srednica;
        return obwod1;
    }

    public float obwod2(float srednica) {
        obwod2 = pi2 * srednica;
        return obwod2;
    }

    /*public float getSrednica() {
        return srednica;
    }

    public void setSrednica(float srednica) {
        this.srednica = srednica;
    }

    public float getPi1() {
        return pi1;
    }

    public void setPi1(float pi1) {
        this.pi1 = pi1;
    }

    public float getPi2() {
        return pi2;
    }

    public void setPi2(float pi2) {
        this.pi2 = pi2;
    }

    public float getObwod() {
        return obwod;
    }

    public void setObwod(float obwod) {
        this.obwod = obwod;
    }

    @Override
    public String toString() {
        return "Pi{" +
                "srednica=" + srednica +
                ", pi1=" + pi1 +
                ", pi2=" + pi2 +
                ", obwod=" + obwod +
                '}';
    }*/
}
